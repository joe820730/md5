#include <iostream>
#include <unistd.h>
#include "md5.hpp"
 
using std::cout; using std::endl;
 
int main(int argc, char *argv[])
{
    int opts = getopt(argc, argv, "f:s:h");
    FILE *fp = nullptr;
    if(opts != -1)
    {
        switch(opts)
        {
            case 'f':
                fp = fopen(optarg, "rb");
                if(fp != nullptr)
                {
                    cout << "md5sum of file " << optarg << ": " << md5(fp) << endl;
                    fclose(fp);
                }
                else
                {
                    cout << "No such file:" << optarg << endl;
                }
                break;
            case 's':
                cout << "md5sum of string " << optarg << ": " << md5(optarg) << endl;
                break;
            case '?':
            case 'h':
            default:
                cout << "Usage: " << argv[0] << " -f [FILE]" << endl;
                cout << "Usage: " << argv[0] << " -s [String]" << endl;
                break;
        }
    }
    else
    {
        cout << "Usage: " << argv[0] << " -f [FILE]" << endl;
        cout << "Usage: " << argv[0] << " -s [String]" << endl;
    }
    return 0;
}