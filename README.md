# MD5 checksum

Based on [This page](http://www.zedwood.com/article/cpp-md5-function)  

I only add functions for calculating file checksum.  
Tested on MinGW and GCC.

### Build:
```
g++ main.cpp md5.cpp -o md5
```

### Usage:
Just copy `md5.cpp` and `md5.hpp` to your project, and use the follow functions:
- `std::string md5(std::string);`  
Input a string, return it's md5 checksum.

- `std::string md5(FILE* fp);`  
Input an opened file, return it's md5 checksum.

### Test program usage:
```
md5 -s [String]
md5 -f [/path/to/file]
```

- Calculating md5 checksum for string "Hello world!" :
> ```bash
> md5 -s "Hello world!"
> ```
> output:
> ```
> md5sum of string Hello world!: 86fb269d190d2c85f6e0468ceca42a20
> ```


- Calculating md5 checksum for files:
> ```bash
> md5 -f main.cpp
> ```
> output:
> ```
> md5sum of file main.cpp: 6743afdcb22de115f37375cdeff8b5cd
> ```

Checked with Linux built-in `md5sum` command.